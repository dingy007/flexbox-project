console.log("Chaining");

var Calc = function (start) {
    var that = this;
    this.add = function (x) {
        console.log('start value in add: ', start, '; x value is: ', x);
        start = start + x;
        return that;
    };
    this.multiply = function (x) {
        console.log('start value in multiply: ', start, '; x value is: ', x);
        start = start * x;
        return that;
    };
    this.equals = function (callback) {
        console.log('start value in equals: ', start, '; callback value is: ', callback);
        callback(start);
        // return that;
    };
};

new Calc(0)
    .add(3)
    .add(4)
    .multiply(2)
    .equals(function (result) {
        console.log(result);
    });
