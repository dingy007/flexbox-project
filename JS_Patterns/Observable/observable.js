// To determine when a property changes -> similar to Observable pattern

var Book = function (name, price) {
    var priceChanging = [];
    var priceChanged = [];
    this.name = function (val) {
        return name;
    };

    this.price = function (val) {
        if (val !== undefined && val !== price) {
            for (var i =0; i<priceChanging.length; i++) {
                if (!priceChanging[i](this, val)) {
                    return price;
                }
            }
            price = val;
            for (var i = 0; i < priceChanged.length; i++) {
                priceChanged[i](this);
            }
        }
        return price;
    };

    this.onPriceChanging = function (callback) {
        priceChanging.push(callback);
    };

    this.onPriceChanged = function (callback) {
        priceChanged.push(callback);
    };
};

var book = new Book('JS Book', 23.99);

console.log('The name is: ', book.name());
console.log('The price is: ', book.price());

book.onPriceChanging(function (bookInstance, newPrice) {
   if (newPrice > 100) {
        console.log('Cannot set value too high.');
        return false;
   }
   return true;
});

book.onPriceChanged(function (bookInstance) {
    console.log('The book price has changed to: $', bookInstance.price());
});

book.price(12.99);

book.price(500);