var Calc = require('./calc.js');

// new Calc(0)
//     .add(3)
//     .add(4)
//     .multiply(2)
//     .equals(function (result) {
//         console.log(result);
//     });

Calc.add(2, 2)
    .multiply(3)
    .equals(function (result) {
        console.log('Result:', result);
    });