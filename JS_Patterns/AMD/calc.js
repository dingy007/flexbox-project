var Calc = function (start) {
    var that = this;
    this.add = function (x) {
        console.log('start value in add: ', start, '; x value is: ', x);
        start = start + x;
        return that;
    };
    this.multiply = function (x) {
        console.log('start value in multiply: ', start, '; x value is: ', x);
        start = start * x;
        return that;
    };
    this.equals = function (callback) {
        console.log('start value in equals: ', start, '; callback value is: ', callback);
        callback(start);
        // return that;
    };
};
// expose the Calc variable to be 'require'd in the other module.
// module.exports = Calc;
module.exports = {
    add: function (x, y) {
        return new Calc(x).add(y || 0);
    },
    multiply: function(x, y) {
        return new Calc(x).multiply(y || 1);
    }
}