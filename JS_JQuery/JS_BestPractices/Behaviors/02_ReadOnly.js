'use strict';
var obj = {};

Object.defineProperty(obj, 'readOnly', {
    enumerable: false,
    configurable: false,
    writable: false,
    value: 'This a read-only var.'
});

console.log(obj.readOnly);