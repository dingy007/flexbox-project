'use strict';

var obj = {
    a: {
        b: {
            c: 'hello'
        }
    }
};

var c = 'trying to over-write c';

(function (valueOfC) {
    console.log(valueOfC);
}(obj.a.b.c))