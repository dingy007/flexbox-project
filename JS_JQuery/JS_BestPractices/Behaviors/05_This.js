'use strict';

var obj = {
    val: 'Hi there',
    printVal: function() {
        console.log(this.val);
    }
};

var obj2 = {
    val: 'Whats up'
};

obj2.printVal = obj.printVal; // use the function printVal in obj, note 'this'

obj2.printVal();

// the below throws exception because the 'this' is not defined
var print = obj.printVal;
// print();

// Binding the print method to obj2
var print2 = obj.printVal.bind(obj2);
print2();

// var obj3 = new obj;
var obj4 = function () {
    var _this = this;
    console.log(this);
    _this.hello = 'hello';

    this.greet = function() {
        console.log(_this.hello);
    };

    this.delayGreeting = function() {
        setTimeout(_this.greet, 1000);
    };
};

var obj5 = new obj4();
console.log(obj5);
obj5.delayGreeting();